


#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "struct_img.h"

enum  write_status  {
    WRITE_OK,
    WRITE_ERROR,
    WRITE_INVALID_PIXELS,
    WRITE_INVALID_BMP_HEADER,
    FILE_FOR_WRITE_NOT_FOUND,
    FAILED_TO_WRITE_PIXEL,
    FAILED_INDENT
    /* коды других ошибок  */
};

enum read_status  {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    FILE_FOR_READ_NOT_FOUND,
    FAILED_TO_READ_PIXEL,
    FAILED_TO_RETREAT
    /* коды других ошибок  */
};



enum read_status from_bmp (FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
