//
// Created by zzzot on 03.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION1_STRUCT_IMG_H
#define ASSIGNMENT_IMAGE_ROTATION1_STRUCT_IMG_H

#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>


struct pixel { uint8_t b, g, r; };

struct resolution {
    uint64_t width, height;
};

struct image {
    struct resolution resolution;
    struct pixel* data;
};


#endif //ASSIGNMENT_IMAGE_ROTATION1_STRUCT_IMG_H
