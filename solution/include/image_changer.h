

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_CHANGER_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_CHANGER_H

#include "struct_img.h"

struct image rotate(const struct image * source );

int32_t get_padding(const struct image * img);

#endif //AASSIGNMENT_IMAGE_ROTATION_IMAGE_CHANGER_H
