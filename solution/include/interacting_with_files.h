
#ifndef ASSIGNMENT_IMAGE_ROTATION_OPEN_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_OPEN_FILE_H



enum close_status {
    CLOSE_OK = 0,
    FILE_NOT_OPENED,
    FILE_NOT_CLOSED

};

enum open_status {
    OPEN_OK,
    FILE_NOT_FOUND,
    FORMAT_ERROR,
    UNKNOWN_OPEN_MODE

};

struct file_input{
    FILE * file;
    enum open_status status;
};

enum fopen_mode {
    READ,
    WRITE,
    APPEND,
    READ_BIN,
    WRITE_BIN,
    APPEND_BIN,
    READ_PLUS,
    WRITE_PLUS,
    APPEND_PLUS,
    R_PLUS_B,
    W_PLUS_B,
    A_PLUS_B,
    R_TEXT,
    W_TEXT,
    A_TEXT,
    R_PLUS_TEXT,
    W_PLUS_TEXT,
    A_PLUS_TEXT
};

struct file_input open_file(const char* path, const enum fopen_mode mode);
enum close_status close_file(FILE* bmp_file);

#endif //ASSIGNMENT_IMAGE_ROTATION_OPEN_FILE_H
