//
// Created by zzzot on 04.01.2023.
//


#ifndef ASSIGNMENT_IMAGE_ROTATION1_ERROR_HANDLER_H
#define ASSIGNMENT_IMAGE_ROTATION1_ERROR_HANDLER_H
#include "bmp.h"
#include "interacting_with_files.h"


int file_open_errors(enum open_status status);
int file_close_errors(enum close_status status);

int bmp_read_errors(enum read_status status);
int bmp_write_errors(enum  write_status status);



#endif //ASSIGNMENT_IMAGE_ROTATION1_ERROR_HANDLER_H
