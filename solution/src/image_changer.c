#include "../include/struct_img.h"

struct resolution get_resolution_for_rotate_90(const struct image* source){
    struct resolution res = {.width = source->resolution.height,.height = source->resolution.width};
    return res;
}


struct pixel* get_linked_data_memory(size_t size){
    return malloc(size);
}

struct pixel* rotate_90(const struct image* source){
    struct pixel* res = get_linked_data_memory(source->resolution.height*source->resolution.width*sizeof(struct pixel));
    for (uint64_t i = 0; i < source->resolution.height*source->resolution.width; i++){
        uint64_t temp_index = source->resolution.width* source->resolution.height - source->resolution.width*(i%source->resolution.height+1) + i/source->resolution.height;
        *(res + i) = *(source->data + temp_index);
    }
    return res;
}


struct image rotate(const struct image* source ){
    struct resolution w_and_h = get_resolution_for_rotate_90(source);
    struct image res = {
            res.resolution = w_and_h,
            res.data = rotate_90(source)
    };
    return res;
}

int32_t get_padding(struct image const* img){
    if((img->resolution.width*sizeof(struct pixel))%4 == 0) return 0;
    else{
        return 4 - (int32_t)(img->resolution.width*sizeof(struct pixel))%4;
    }

}
