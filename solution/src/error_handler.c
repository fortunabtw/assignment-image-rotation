//
// Created by zzzot on 04.01.2023.
//
#include "error_handler.h"
#include "bmp.h"
#include "interacting_with_files.h"



int file_open_errors(enum open_status status){
    switch (status) {
        case OPEN_OK:
            fputs("The file was successfully opened", stdout);
            return 0;
        case FILE_NOT_FOUND:
            fputs("Could not find file", stderr);
            return -1;
        case FORMAT_ERROR:
            fputs("File format error", stderr);
            return -1;
        case UNKNOWN_OPEN_MODE:
            fputs("Unknown modifier to open", stderr);
            return -1;
        default:
            fputs("Unknown error", stderr);
            return -1;
    }
}

int file_close_errors(enum close_status status) {
    switch (status) {
        case CLOSE_OK:
            fputs("The file was successfully closed", stdout);
            return 0;
        case FILE_NOT_CLOSED:
            fputs("The file has not been closed", stderr);
            return -1;
        case FILE_NOT_OPENED:
            fputs("The file was not opened", stderr);
            return -1;
        default:
            fputs("Unknown error", stderr);
            return -1;
    }
}

int bmp_read_errors(enum read_status status){
    switch (status) {
        case READ_OK:
            fputs("The file was successfully read", stdout);
            return 0;
        case READ_INVALID_SIGNATURE:
            fputs("Read invalid signature", stderr);
            return -1;
        case READ_INVALID_BITS:
            fputs("Read invalid bits", stderr);
            return -1;
        case READ_INVALID_HEADER:
            fputs("Read invalid header", stderr);
            return -1;
        case FILE_FOR_READ_NOT_FOUND:
            fputs("File for read not found", stderr);
            return -1;
        case FAILED_TO_READ_PIXEL:
            fputs("Failed to read pixel", stderr);
            return -1;
        case FAILED_TO_RETREAT:
            fputs("Failed to retreat", stderr);
            return -1;
        default:
            fputs("Unknown error", stderr);
            return -1;
    }
}

int bmp_write_errors(enum  write_status status){
    switch (status) {
        case WRITE_OK:
            fputs("The file was successfully write", stdout);
            return 0;
        case WRITE_ERROR:
            fputs("Write error", stderr);
            return -1;
        case WRITE_INVALID_PIXELS:
            fputs("Write invalid pixels", stderr);
            return -1;
        case WRITE_INVALID_BMP_HEADER:
            fputs("Write invalid bmp_header", stderr);
            return -1;
        case FILE_FOR_WRITE_NOT_FOUND:
            fputs("File for write not found", stderr);
            return -1;
        case FAILED_TO_WRITE_PIXEL:
            fputs("Failed to write pixel", stderr);
            return -1;
        case FAILED_INDENT:
            fputs("Failed indent", stderr);
            return -1;
        default:
            fputs("Unknown error", stderr);
            return -1;

    }
}
