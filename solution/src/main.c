#include "../include/struct_bmp_header.h"
#include "../include/error_handler.h"
#include "../include/image_changer.h"
#include "../include/interacting_with_files.h"

int main( int argc, char** argv ) {

    if (argc != 3) {
        fputs("incorrect number of arguments\n",stderr);
        return -1;
    }
    struct file_input in_file = open_file(argv[1], READ_BIN);
    if (file_open_errors(in_file.status) != 0){
        fclose(in_file.file);
        return -1;
    }
    FILE * in = in_file.file;


    struct file_input out_file = open_file(argv[2], WRITE_BIN);
    if (file_open_errors(out_file.status) != 0){
        fclose(out_file.file);
        return -1;
    }
    FILE * out = out_file.file;



    struct image* img = malloc(sizeof(struct image));
    enum read_status read = from_bmp(in,img);
    if (bmp_read_errors(read) != 0){
        fclose(in);
        fclose(out);
        free(img->data);
        free(img);
        return read;
    }

    struct image* res = malloc(sizeof(struct image));
    *res = rotate(img);

    enum write_status write =  to_bmp(out,res);
    bmp_write_errors(write);

    free(img->data);
    free(img);
    free(res->data);
    free(res);


    enum close_status closeStatus = close_file(in);
    file_close_errors(closeStatus);

    enum close_status closeStatus1 = close_file(out);
    file_close_errors(closeStatus1);

    return write;
}
