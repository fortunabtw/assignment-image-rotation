//
// Created by zzzot on 20.10.2022.
//


#include "../include/struct_bmp_header.h"
#include "../include/bmp.h"
#include "../include/image_changer.h"


struct bmp_header create_header(struct image const* source){
    struct bmp_header res;


    uint32_t padding = get_padding(source);

    res.bfType = 0x4D42;
    res.bfileSize = sizeof(struct bmp_header) + (source->resolution.width)*source->resolution.height*sizeof(struct pixel)+padding*source->resolution.width;
    res.bfReserved = 0;
    res.bOffBits = sizeof(struct bmp_header);
    res.biSize=40;
    res.biWidth = source->resolution.width;
    res.biHeight = source->resolution.height;
    res.biPlanes = 1;
    res.biBitCount = 24;
    res.biCompression = 0;
    res.biSizeImage = source->resolution.height * (source->resolution.width)*sizeof(struct pixel) + padding*source->resolution.width;
    res.biXPelsPerMeter = 2834;
    res.biYPelsPerMeter = 2834;
    res.biClrUsed = 0;
    res.biClrImportant = 0;
    return res;
}

/*  deserializer   */


enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header f;
    if(in == NULL){
        return FILE_FOR_READ_NOT_FOUND;
    }
    if (fread(&f, sizeof(struct bmp_header),1,in) != 1){
        return READ_INVALID_HEADER;
    }

    img->resolution.height = f.biHeight;
    img->resolution.width = f.biWidth;
    img->data = malloc(sizeof(struct pixel)*img->resolution.width*img->resolution.height);

    const int32_t padding = get_padding(img);

    uint64_t read_pixels_counter = 0;

    for (uint64_t h = 0; h < (img->resolution.height); h ++){
        for (uint64_t w = 0; w < (img->resolution.width); w ++){
            size_t iteration_read_pixels = fread(&((img->data)[h*img->resolution.width+w]),sizeof(struct pixel),1,in);
            if (iteration_read_pixels != 1){
                return FAILED_TO_READ_PIXEL;
            }
            read_pixels_counter += iteration_read_pixels;
        }
        if(fseek(in,padding,SEEK_CUR) != 0){
            return FAILED_TO_RETREAT;
        }
    }
    if(read_pixels_counter != img->resolution.height*img->resolution.width) {
        return READ_INVALID_BITS;
    } else {
        return READ_OK;
    }
}


/*  serializer   */

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = create_header(img);
    if(out == NULL) {
        return FILE_FOR_WRITE_NOT_FOUND;
    }
    if(fwrite(&header,sizeof(struct bmp_header),1,out) != 1){
        return WRITE_INVALID_BMP_HEADER;
    }
    const int32_t padding_t = get_padding(img);
    const char padding_zero[4]  = {0};
    uint64_t wrote_pixels_counter = 0;

    //fseek(out, sizeof(struct bmp_header),SEEK_SET);

    for (uint64_t h = 0; h < (img->resolution.height); h ++) {
        for (uint64_t w = 0; w < (img->resolution.width); w++) {
            size_t iteration_wrote_pixel = fwrite(&((img->data)[h * img->resolution.width + w]), sizeof(struct pixel), 1, out);
            if (iteration_wrote_pixel != 1){
                return FAILED_TO_WRITE_PIXEL;
            }
            wrote_pixels_counter += iteration_wrote_pixel;
        }
        if(fwrite(&padding_zero, padding_t, 1, out) != 1){
            return FAILED_INDENT;
        }
    }
    if (wrote_pixels_counter != img->resolution.height*img->resolution.width){
        return WRITE_INVALID_PIXELS;
    } else {
        return WRITE_OK;
    }

}

