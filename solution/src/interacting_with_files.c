//
// Created by zzzot on 17.10.2022.
//
#include "stdio.h"
#include "../include/interacting_with_files.h"
#include "interacting_with_files.h"



struct file_input open_file(const char* path, const enum fopen_mode mode){
    FILE * bmp_file = NULL;
    enum open_status open_stat;
    switch (mode) {
        case READ:
            bmp_file = fopen(path, "r");
            break;
        case WRITE:
            bmp_file = fopen(path, "w");
            break;
        case APPEND:
            bmp_file = fopen(path, "a");
            break;
        case READ_BIN:
            bmp_file = fopen(path, "rb");
            break;
        case WRITE_BIN:
            bmp_file = fopen(path, "wb");
            break;
        case APPEND_BIN:
            bmp_file = fopen(path, "ab");
            break;
        case READ_PLUS:
            bmp_file = fopen(path, "r+");
            break;
        case WRITE_PLUS:
            bmp_file = fopen(path, "w+");
            break;
        case APPEND_PLUS:
            bmp_file = fopen(path, "a+");
            break;
        case R_PLUS_B:
            bmp_file = fopen(path, "r+b");
            break;
        case W_PLUS_B:
            bmp_file = fopen(path, "w+b");
            break;
        case A_PLUS_B:
            bmp_file = fopen(path, "a+b");
            break;
        case R_TEXT:
            bmp_file = fopen(path, "rt");
            break;
        case W_TEXT:
            bmp_file = fopen(path, "wt");
            break;
        case A_TEXT:
            bmp_file = fopen(path, "at");
            break;
        case R_PLUS_TEXT:
            bmp_file = fopen(path, "r+t");
            break;
        case W_PLUS_TEXT:
            bmp_file = fopen(path, "w+t");
            break;
        case A_PLUS_TEXT:
            bmp_file = fopen(path, "a+t");
            break;
        default:
            open_stat = UNKNOWN_OPEN_MODE;
            break;
    }
    struct file_input f;
    if (!bmp_file) {
        open_stat = FILE_NOT_FOUND;
        f.status = open_stat;
    } else {
        open_stat = OPEN_OK;
        f.status = open_stat;
        f.file = bmp_file;
    }

    return f;

}




enum close_status close_file(FILE* bmp_file){
    fclose(bmp_file);
    if (bmp_file) return FILE_NOT_CLOSED;
    return CLOSE_OK;
}



